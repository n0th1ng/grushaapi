from rest_framework import generics
from grusha.models import Product, Category, CallBack, Promotion, Comment, Client, Order
from .serializers import (ProductSerializer, CategoryListSerializer, CommentSerializer,
                          CategoryDetailSerializer, CallbackSerializer, PromotionSerializer,
                          ClientSerializer, OrderSerializer)


class ProductListApiView(generics.ListAPIView):
    queryset = Product.objects.filter(available=True)
    serializer_class = ProductSerializer

    def get_queryset(self):
        queryset = super(ProductListApiView, self).get_queryset()
        if self.request.GET.get('id', None):
            queryset = queryset.filter(category_id=self.request.GET.get('id', None))
        return queryset


class ProductDetailApiView(generics.RetrieveAPIView):
    queryset = Product.objects.filter(available=True)
    serializer_class = ProductSerializer


class CategoryListApiView(generics.ListAPIView):
    queryset = Category.objects.filter(active=True)
    serializer_class = CategoryListSerializer


class CategoryDetailApiView(generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryDetailSerializer


class CallbackCreateApiView(generics.CreateAPIView):
    serializer_class = CallbackSerializer
    queryset = CallBack.objects.all()


class PromotionListApiView(generics.ListAPIView):
    queryset = Promotion.objects.filter(active=True)
    serializer_class = PromotionSerializer


class CommentListApiView(generics.ListAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class ClientsListApiView(generics.ListAPIView):
    queryset = Client.objects.filter(active=True)
    serializer_class = ClientSerializer


class OrderCreateApiView(generics.CreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


