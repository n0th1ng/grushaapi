from django.db import models
from .validators import validate_tel, validate_amount


class Category(models.Model):
    name = models.CharField(max_length=50)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name_plural = 'categories'


class Product(models.Model):
    name = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='products')
    color = models.ManyToManyField('Color')
    image = models.ImageField(upload_to='product_images/%Y/%m/%d/')
    price = models.PositiveIntegerField()
    payoff = models.PositiveSmallIntegerField(default=0)
    actual_price = models.PositiveIntegerField(blank=True)
    available = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return f'{self.name} {self.category}'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.actual_price = self.price * (1 - self.payoff / 100)
        super().save(force_insert=False, force_update=False, using=None, update_fields=None)

    class Meta:
        unique_together = ('name', 'category')


class CallBack(models.Model):
    name = models.CharField(max_length=50)
    tel = models.CharField(max_length=13, validators=[validate_tel])
    called = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name} {self.tel}'

    class Meta:
        unique_together = ('name', 'tel')


class Color(models.Model):
    name = models.CharField(max_length=20)
    color_code = models.CharField(max_length=7)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.name}'


class Order(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    color = models.ForeignKey(Color, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    tel = models.CharField(max_length=13, validators=[validate_tel])
    amount = models.PositiveSmallIntegerField(validators=[validate_amount])
    called = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Client(models.Model):
    name = models.CharField(max_length=50, unique=True)
    image = models.ImageField(upload_to='client_images')
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name}'


class ProductComments(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    comment = models.TextField()
    address = models.CharField(max_length=50)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Promotion(models.Model):
    body = models.TextField()
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Comment(models.Model):
    customer = models.CharField(max_length=75)
    body = models.TextField()
    address = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.customer} ---- {self.address}'
