from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_tel(value):
    if not value.isdigit() or len(value) != 12:
        raise ValidationError(_('%(value)s is not valid'), params={'value': value})


def validate_amount(value):

    if 50 < value < 1:
        raise ValidationError(_('%(value)s is not valid'), params={'value': value})
