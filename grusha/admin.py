from django.contrib import admin
from .models import Category, Product, Color, CallBack, Order, Promotion, Comment, Client
from .forms import ColorModelForm


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at', 'updated_at', 'active')
    list_editable = ('active', )
    search_fields = ('name',)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'image', 'price', 'payoff', 'actual_price', 'available')
    list_display_links = ('category', 'name',)
    list_editable = ('available',)
    autocomplete_fields = ('category',)
    readonly_fields = ('actual_price',)
    filter_horizontal = ('color',)


@admin.register(Color)
class ColorAdmin(admin.ModelAdmin):
    list_display = ('name', 'active')
    list_editable = ('active',)
    form = ColorModelForm


@admin.register(CallBack)
class CallBackAdmin(admin.ModelAdmin):
    list_display = ('name', 'tel', 'created_at', 'called')
    list_editable = ('called',)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('name', 'tel', 'product', 'color', 'amount', 'called')
    list_display_links = ('product',)
    list_editable = ('called',)


@admin.register(Promotion)
class PromotionAdmin(admin.ModelAdmin):
    list_display = ('body', 'active')


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('customer', 'body', 'address')


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'image', 'active', 'created_at', 'updated_at')
